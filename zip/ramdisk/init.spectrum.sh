#!/system/bin/sh
# SPECTRUM KERNEL MANAGER
# Profile initialization script by nathanchance, Zile995 and SJD and Hzn

value=`getprop persist.spectrum.profile`

# Set CPU profile (Some rom developers like to mess with these settings, so make sure these settings are applied properly)


if [[ $value -eq 0 ]]; then
	chmod 644 /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	echo "intelliactive" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	chmod 644 /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
	echo "0:2016000" > /sys/module/msm_performance/parameters/cpu_max_freq
    echo "2016000" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
    echo "650000000" > /sys/class/kgsl/kgsl-3d0/max_gpuclk
	echo "msm-adreno-tz" > /sys/class/kgsl/kgsl-3d0/devfreq/governor
	chmod 0664 /sys/module/workqueue/parameters/power_efficient
    echo "Y" > /sys/module/workqueue/parameters/power_efficient
elif [[ $value -eq 1 ]]; then
	chmod 644 /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	echo "performance" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	chmod 644 /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
	echo "0:2208000" > /sys/module/msm_performance/parameters/cpu_max_freq
    echo "2208000" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
    echo "700000000" > /sys/class/kgsl/kgsl-3d0/max_gpuclk
	echo "performance" > /sys/class/kgsl/kgsl-3d0/devfreq/governor
	chmod 0664 /sys/module/workqueue/parameters/power_efficient
    echo "N" > /sys/module/workqueue/parameters/power_efficient
elif [[ $value -eq 2 ]]; then
	chmod 644 /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	echo "interactive" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	chmod 644 /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
	echo "0:1689600" > /sys/module/msm_performance/parameters/cpu_max_freq
    echo "1689600" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
    echo "510000000" > /sys/class/kgsl/kgsl-3d0/max_gpuclk
	echo "msm-adreno-tz" > /sys/class/kgsl/kgsl-3d0/devfreq/governor
	chmod 0664 /sys/module/workqueue/parameters/power_efficient
    echo "Y" > /sys/module/workqueue/parameters/power_efficient
else [[ $value -eq 3 ]];
	chmod 644 /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	echo "darkness" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	chmod 644 /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
	echo "0:2208000" > /sys/module/msm_performance/parameters/cpu_max_freq
    echo "2208000" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
    echo "700000000" > /sys/class/kgsl/kgsl-3d0/max_gpuclk
	echo "msm-adreno-tz" > /sys/class/kgsl/kgsl-3d0/devfreq/governor
	chmod 0664 /sys/module/workqueue/parameters/power_efficient
    echo "N" > /sys/module/workqueue/parameters/power_efficient
fi
